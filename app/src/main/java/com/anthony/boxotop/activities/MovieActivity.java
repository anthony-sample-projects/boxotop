package com.anthony.boxotop.activities;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.anthony.boxotop.R;
import com.anthony.boxotop.fragments.MovieFragment;
import com.anthony.boxotop.fragments.MovieListFragment;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

public class MovieActivity extends AppCompatActivity implements HasSupportFragmentInjector {
    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingAndroidInjector;

    private static final String MOVIE_ID = "movieID";
    private int mMovieID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);
        mMovieID = getIntent().getIntExtra(MOVIE_ID,-1);
        this.configureDagger();
        this.showFragment(savedInstanceState);
    }

    @Override
    public DispatchingAndroidInjector<Fragment> supportFragmentInjector() {
        return dispatchingAndroidInjector;
    }
    private void configureDagger(){
        AndroidInjection.inject(this);
    }

    private void showFragment(Bundle savedInstanceState){
        if (savedInstanceState == null) {

            MovieFragment fragment = new MovieFragment();

            Bundle bundle = new Bundle();
            bundle.putInt(MOVIE_ID, mMovieID);
            fragment.setArguments(bundle);

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.activity_movie_container, fragment, null)
                    .commit();
        }
    }


}
