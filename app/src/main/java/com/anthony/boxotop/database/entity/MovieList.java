package com.anthony.boxotop.database.entity;

import android.arch.persistence.room.Entity;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MovieList {

    @SerializedName("results")
    private ArrayList<Movie> movies;

    public ArrayList<Movie> getMovieArrayList() {
        return movies != null ? movies : new ArrayList<>();
    }

    public void setMovies(ArrayList<Movie> movies) {
        this.movies = movies;
    }
}
