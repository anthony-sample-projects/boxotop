package com.anthony.boxotop.di.module;

import com.anthony.boxotop.activities.MovieActivity;
import com.anthony.boxotop.activities.MovieListActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityModule {
    @ContributesAndroidInjector(modules = FragmentModule.class)
    abstract MovieListActivity contributeMovieListActivity();

    @ContributesAndroidInjector(modules = FragmentModule.class)
    abstract MovieActivity contributeMovieActivity();
}