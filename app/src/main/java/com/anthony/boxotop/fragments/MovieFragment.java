package com.anthony.boxotop.fragments;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.anthony.boxotop.R;
import com.anthony.boxotop.database.entity.Movie;
import com.anthony.boxotop.viewmodels.MovieViewModel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.AndroidSupportInjection;

public class MovieFragment extends Fragment {
    private static final String MOVIE_ID = "movieID";

    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private MovieViewModel viewModel;

    private Movie movieSelected;

    @BindView(R.id.movie_poster) ImageView moviePoster;
    @BindView(R.id.movie_title) TextView movieTitle;
    @BindView(R.id.movie_year) TextView movieYear;
    @BindView(R.id.movie_homepage) TextView movieHomepage;
    @BindView(R.id.movie_overview) TextView movieOverview;
    @BindView(R.id.movie_tagline) TextView movieTagline;

    public MovieFragment(){ }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstance){
        View view = inflater.inflate(R.layout.fragment_movie, container, false);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.configureDagger();
        this.configureViewModel();
    }

    private void configureDagger(){
        AndroidSupportInjection.inject(this);
    }

    private void configureViewModel(){
        int movieId = getArguments().getInt(MOVIE_ID);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MovieViewModel.class);
        viewModel.init(movieId);
        viewModel.getMovie().observe(this, movie -> updateUI(movie));
    }

    private void updateUI(@Nullable Movie movie){
        if (movie != null){
            this.movieSelected = movie;
            Glide.with(this).load(movie.getPosterStringURL(movie.getPosterPath())).into(moviePoster);
            this.movieTitle.setText(movie.getTitle());
            this.movieOverview.setText(movie.getOverview());
            this.movieYear.setText(movie.getReleaseDate());
            this.movieHomepage.setText(movie.getHomepage());
            this.movieTagline.setText(movie.getTagline());
        }
    }
}
