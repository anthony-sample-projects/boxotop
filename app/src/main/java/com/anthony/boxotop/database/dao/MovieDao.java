package com.anthony.boxotop.database.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;
import com.anthony.boxotop.database.entity.Movie;
import com.anthony.boxotop.database.entity.MovieList;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface MovieDao {
    @Insert(onConflict = REPLACE)
    void save(List<Movie> movie);

    @Insert(onConflict = REPLACE)
    void saveMovie(Movie movie);

    @Query("SELECT * FROM movie WHERE id = :movieId")
    LiveData<Movie> load(int movieId);

    @Query("SELECT * FROM movie")
    LiveData<List<Movie>> loadAll();

    @Query("SELECT * FROM movie WHERE id = :movieId")
    Movie hasMovie(int movieId);
}
