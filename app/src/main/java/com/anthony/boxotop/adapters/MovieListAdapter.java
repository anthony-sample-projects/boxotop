package com.anthony.boxotop.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.anthony.boxotop.R;
import com.anthony.boxotop.database.entity.Movie;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieListAdapter extends RecyclerView.Adapter<MovieListAdapter.MovieViewHolder>{

    private Context mContext;
    private ArrayList<Movie> movieList;

    public MovieListAdapter(Context context, ArrayList<Movie> movieList) {
        this.mContext = context;
        this.movieList = movieList;
    }

    @NonNull
    @Override
    public MovieListAdapter.MovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_movie_list, parent, false);
        return new MovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieViewHolder movieViewHolder, int i) {
        Movie movie = movieList.get(i);

        //Title
        movieViewHolder.mMovieTitle.setText(movie.getTitle());

        //Poster
        ImageView poster = movieViewHolder.mMoviePoster;
        Glide.with(mContext).load(movie.getPosterStringURL(movie.getPosterPath())).into(poster);

        //Details..
        movieViewHolder.mMovieRating.setText(movie.getVoteAverage());

        //Alternated background color
        if ((i % 2) == 0) {
            movieViewHolder.mContainer.setBackgroundColor(ContextCompat.getColor(
                    mContext,R.color.single_movie_green));
        } else {
            movieViewHolder.mContainer.setBackgroundColor(ContextCompat.getColor(
                    mContext,R.color.single_movie_white));
        }
    }

    public Movie getMovie(int position){
        return this.movieList.get(position);
    }

    public void updateSearchList(List<Movie> movies){
        movieList = new ArrayList<>();
        movieList.addAll(movies);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

    public class MovieViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        @BindView(R.id.movie_single_name) TextView mMovieTitle;
        @BindView(R.id.movie_single_infos) TextView mMovieRating;
        @BindView(R.id.movie_single_image) ImageView mMoviePoster;
        @BindView(R.id.movie_single_container) RelativeLayout mContainer;


        public MovieViewHolder(View itemView){
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(this);
        }

        public void onClick(View view) {

        }
    }
}
