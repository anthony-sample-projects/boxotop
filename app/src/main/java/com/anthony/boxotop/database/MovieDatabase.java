package com.anthony.boxotop.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.anthony.boxotop.database.converter.DateConverter;
import com.anthony.boxotop.database.dao.MovieDao;
import com.anthony.boxotop.database.entity.Movie;

@Database(entities = {Movie.class}, version = 1)
@TypeConverters(DateConverter.class)
public abstract class MovieDatabase extends RoomDatabase {

    // --- SINGLETON ---
    private static volatile MovieDatabase INSTANCE;

    // --- DAO ---
    public abstract MovieDao movieDao();

}
