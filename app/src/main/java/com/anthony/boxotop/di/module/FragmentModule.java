package com.anthony.boxotop.di.module;

import com.anthony.boxotop.fragments.MovieFragment;
import com.anthony.boxotop.fragments.MovieListFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentModule {
    @ContributesAndroidInjector
    abstract MovieListFragment contributeMovieListFragment();

    @ContributesAndroidInjector
    abstract MovieFragment contributeMovieFragment();
}
