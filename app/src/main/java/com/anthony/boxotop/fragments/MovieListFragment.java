package com.anthony.boxotop.fragments;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.anthony.boxotop.R;
import com.anthony.boxotop.activities.MovieActivity;
import com.anthony.boxotop.adapters.MovieListAdapter;
import com.anthony.boxotop.database.entity.Movie;
import com.anthony.boxotop.utils.ItemClickSupport;
import com.anthony.boxotop.viewmodels.MovieListViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.AndroidSupportInjection;

public class MovieListFragment extends Fragment {
   private RecyclerView moviesList;

    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private MovieListViewModel viewModel;

    private ArrayList<Movie> movieList;
    private MovieListAdapter movieListAdapter;

    @BindView(R.id.moviesRecyclerView) RecyclerView movieRecyclerView;
    @BindView(R.id.movie_list_swipeRefreshContainer) SwipeRefreshLayout swipeRefreshLayout;

    public MovieListFragment() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstance){
        View view = inflater.inflate(R.layout.fragment_movie_list, container, false);
        ButterKnife.bind(this,view);
        this.configureRecyclerView();
        this.configureDagger();
        this.configureViewModel();
        this.configureSwipeRefreshLayout();
        this.configureOnClickRecyclerView();
        return view;
    }

    // -----------------
    // CONFIGURATION
    // -----------------

    //Configure RecyclerView, Adapter, LayoutManager & glue it together
    private void configureRecyclerView(){
        // Reset list
        this.movieList = new ArrayList<>();
        //Create adapter passing the list of users
        this.movieListAdapter = new MovieListAdapter(getContext(),this.movieList);
        //Attach the adapter to the recyclerview to populate items
        this.movieRecyclerView.setAdapter(this.movieListAdapter);
        //Set layout manager to position the items
        this.movieRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void configureDagger(){
        AndroidSupportInjection.inject(this);
    }

    private void configureSwipeRefreshLayout() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Toast toast = Toast.makeText(getContext(),"Movie list refreshed",Toast.LENGTH_SHORT);
                configureViewModel();
                toast.show();
            }
        });
    }

    private void configureViewModel(){
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MovieListViewModel.class);
        viewModel.init();
        viewModel.getMovies().observe(this, movies -> updateUI(movies));
    }

    private void updateUI(@Nullable List<Movie> movies){
        if (movies != null){
            swipeRefreshLayout.setRefreshing(false);
            movieList.clear();
            movieList.addAll(movies);
            movieListAdapter.notifyDataSetChanged();
        }
    }

    private void configureOnClickRecyclerView(){
        ItemClickSupport.addTo(movieRecyclerView, R.layout.single_item_movie_list)
                .setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                    @Override
                    public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                        Movie movieClicked = movieListAdapter.getMovie(position);
                        Toast.makeText(getContext(), "You clicked on movie : "+movieClicked.getId()+" title : "+movieClicked.getTitle(), Toast.LENGTH_SHORT).show();
                        Intent movieIntent = new Intent(getContext(), MovieActivity.class);
                        movieIntent.putExtra("movieID", movieClicked.getId());
                        startActivity(movieIntent);
                    }
                });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        inflater.inflate(R.menu.toolbar_menu,menu);
        MenuItem menuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView)menuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String input) {
                String userInput = input.toLowerCase();
                List<Movie> newMovieList = new ArrayList<>();
                for(Movie movie : movieList){
                    if(movie.getTitle().toLowerCase().contains(userInput)){
                        newMovieList.add(movie);
                    }
                }
                movieListAdapter.updateSearchList(newMovieList);
                return true;
            }
        });
        super.onCreateOptionsMenu(menu,inflater);
    }
}