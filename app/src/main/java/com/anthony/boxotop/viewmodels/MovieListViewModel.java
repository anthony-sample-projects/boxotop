package com.anthony.boxotop.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import com.anthony.boxotop.database.entity.Movie;
import com.anthony.boxotop.repositories.MovieRepository;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class MovieListViewModel extends ViewModel {
    private LiveData<List<Movie>> movies;
    private MovieRepository movieRepo;

    @Inject
    public MovieListViewModel(MovieRepository movieRepo) {
        this.movieRepo = movieRepo;
    }

    // ----

    public void init() {
        if (this.movies != null) {
            return;
        }
        movies = movieRepo.getMovies();
    }

    public LiveData<List<Movie>> getMovies() {
        return this.movies;
    }
}
