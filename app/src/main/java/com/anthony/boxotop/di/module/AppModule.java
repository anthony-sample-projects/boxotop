package com.anthony.boxotop.di.module;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.anthony.boxotop.api.MovieWebservice;
import com.anthony.boxotop.database.MovieDatabase;
import com.anthony.boxotop.database.dao.MovieDao;
import com.anthony.boxotop.repositories.MovieRepository;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(includes = ViewModelModule.class)
public class AppModule {

    // --- DATABASE INJECTION ---

    @Provides
    @Singleton
    MovieDatabase provideDatabase(Application application) {
        return Room.databaseBuilder(application,
                MovieDatabase.class, "MovieDatabase.db")
                .build();
    }

    @Provides
    @Singleton
    MovieDao provideUserDao(MovieDatabase database) { return database.movieDao(); }

    // --- REPOSITORY INJECTION ---

    @Provides
    Executor provideExecutor() {
        return Executors.newSingleThreadExecutor();
    }

    @Provides
    @Singleton
    MovieRepository provideUserRepository(MovieWebservice webservice, MovieDao userDao, Executor executor) {
        return new MovieRepository(webservice, userDao, executor);
    }

    // --- NETWORK INJECTION ---

    private static String BASE_URL = "https://api.themoviedb.org/";

    @Provides
    Gson provideGson() { return new GsonBuilder().create(); }

    @Provides
    Retrofit provideRetrofit(Gson gson) {
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(BASE_URL)
                .build();
        return retrofit;
    }

    @Provides
    @Singleton
    MovieWebservice provideApiWebservice(Retrofit restAdapter) {
        return restAdapter.create(MovieWebservice.class);
    }
}