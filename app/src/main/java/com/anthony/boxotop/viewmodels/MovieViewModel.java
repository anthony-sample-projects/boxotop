package com.anthony.boxotop.viewmodels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.anthony.boxotop.database.entity.Movie;
import com.anthony.boxotop.repositories.MovieRepository;

import java.util.List;

import javax.inject.Inject;

public class MovieViewModel extends ViewModel {
    private LiveData<Movie> movie;
    private MovieRepository movieRepo;

    @Inject
    public MovieViewModel(MovieRepository movieRepo) {
        this.movieRepo = movieRepo;
    }

    // ----

    public void init(int movieId) {
        if (this.movie != null) {
            return;
        }
        movie = movieRepo.getMovie(movieId);
    }

    public LiveData<Movie> getMovie() {
        return this.movie;
    }
}
