package com.anthony.boxotop.repositories;

import android.arch.lifecycle.LiveData;
import android.util.Log;
import android.widget.Toast;

import com.anthony.boxotop.api.MovieWebservice;
import com.anthony.boxotop.database.dao.MovieDao;
import com.anthony.boxotop.database.entity.Movie;
import com.anthony.boxotop.database.entity.MovieList;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executor;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@Singleton
public class MovieRepository {
    private static int FRESH_TIMEOUT_IN_MINUTES = 1;

    private final MovieWebservice webservice;
    private final MovieDao movieDao;
    private final Executor executor;

    @Inject
    public MovieRepository(MovieWebservice webservice, MovieDao movieDao, Executor executor) {
        this.webservice = webservice;
        this.movieDao = movieDao;
        this.executor = executor;
    }

    public LiveData<List<Movie>> getMovies() {
        refreshMovies(); // try to refresh data if possible from TheMovieDBApI
        return movieDao.loadAll(); // return a LiveData directly from the database.
    }

    public LiveData<Movie> getMovie(int movieId) {
        refreshMovie(movieId); // try to refresh data if possible from TheMovieDBApI
        return movieDao.load(movieId); // return a LiveData directly from the database.
    }
    private void refreshMovie(int movieId) {
        executor.execute(() -> {
            // Check if movies were fetched recently
            boolean movieExists = (movieDao.hasMovie(movieId) != null);
            // If movie have to be updated
            if (!movieExists) {
            webservice.getMovie(movieId).enqueue(new Callback<Movie>() {
                @Override
                public void onResponse(Call<Movie> call, Response<Movie> response) {
                    Log.e("MovieRepository", "DATA REFRESHED FROM NETWORK");
                    //Toast.makeText(App.context, "Data refreshed from network !", Toast.LENGTH_LONG).show();
                    executor.execute(() -> {
                        Movie movie = response.body();
                        movieDao.saveMovie(movie);
                    });
                }

                @Override
                public void onFailure(Call<Movie> call, Throwable t) {
                    Log.e("MovieRepository", "DATA REFRESHED FROM NETWORK");
                }
            });
    }});}

    private void refreshMovies() {
        executor.execute(() -> {
            // TODO check if movies were fetched recently and if lastRefresh < duration
            //if (moviesShouldBeRefreshedFromNetwork) {
                webservice.getNowPlayingMovies(1).enqueue(new Callback<MovieList>() {
                    @Override
                    public void onResponse(Call<MovieList> call, Response<MovieList> response) {
                        Log.e("MovieRepository", "DATA REFRESHED FROM NETWORK");
                        //Toast.makeText(App.context, "Data refreshed from network !", Toast.LENGTH_LONG).show();
                        executor.execute(() -> {
                            ArrayList<Movie> movies = response.body().getMovieArrayList();
                            movieDao.save(movies);
                        });
                    }

                    @Override
                    public void onFailure(Call<MovieList> call, Throwable t) {
                        Log.e("MovieRepository", "DATA REFRESHED FROM NETWORK");
                    }
                });
            //}
        });
    }

    // ---

    private Date getMaxRefreshTime(Date currentDate){
        Calendar cal = Calendar.getInstance();
        cal.setTime(currentDate);
        cal.add(Calendar.MINUTE, -FRESH_TIMEOUT_IN_MINUTES);
        return cal.getTime();
    }
}
