package com.anthony.boxotop.api;

import com.anthony.boxotop.database.entity.Movie;
import com.anthony.boxotop.database.entity.MovieList;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MovieWebservice {
    String tmdbApiKey = "?api_key=001ecfe4e533594a84f831f70097379a";

    @GET("/3/movie/now_playing"+tmdbApiKey)
    Call<MovieList> getNowPlayingMovies(@Query("page") int pageNumber);

    @GET("/3/movie/{movie_id}"+tmdbApiKey)
    Call<Movie> getMovie(@Path("movie_id") int idMovie);
}
